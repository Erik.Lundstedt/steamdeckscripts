#!/usr/bin/env fennel
(local shabang "#!/usr/bin/env bash")

(fn note [content]
  "creates a sh comment"
  (table.concat ["\n" "#" content ] )
  )




(local setup
	   (table.concat
		[
		 ""
		 ""
		 ""
		 ]
		"\n")
	   )
(fn install [packages]
  "takes in a list of packagenames and outputs apt-get commands to install said packages"
  (local items [])
  (each [_ v (ipairs packages)]
	(table.insert items (.. "paru -S " v ))
	)
  (table.concat items "\n")
  )
(local packages
	   [""
		""
		]
	   )




(fn notes [notelist]
  (local a [])
  (each [key value (ipairs notelist)]
	(tset a key (note value))
	)
  (table.concat a )
  )

(fn license []
  (notes
   []
   )

  )




(local commands
		[
		shabang
		(notes
			[
			"A list of common commands can be found at https://erik.lundstedt.it/pages/jamjamsCommands.html"
			]
		)
		(note "fetch data from repositories and create group")
		setup
		(note "install needed packages")
		(install packages)
		]
	   )
(print (table.concat commands "\n"))
