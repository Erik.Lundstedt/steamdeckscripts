#!/usr/bin/env bash
distrobox-create -i ghcr.io/greyltc-org/archlinux-aur:paru -n tic80

distrobox-enter tic80 -- paru -S tic-80-git
